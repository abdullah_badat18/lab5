package linearalgebra;
public class Vector3d{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){

        return Math.sqrt((this.x * this.x) + (this.y * this.y)
         + (this.z * this.z));
    }

    public double dotProduct(Vector3d vector){
        return (this.x * vector.getX()) + (this.y * vector.getY())
         + (this.z * vector.getZ());
    }

    public Vector3d add(Vector3d vector){
        double newX = this.x + vector.getX();
        double newY = this.y + vector.getY();
        double newZ = this.z + vector.getZ();
        return new Vector3d(newX, newY, newZ);
    }

    public static void main (String[]args){
        Vector3d vector= new Vector3d(4.0,3.0, 1);
    
        System.out.println(vector.getZ());
    } 
}