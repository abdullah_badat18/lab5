package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Vector3dTests{
    public static void main(String[] args )
    {
        System.out.println( "Hello World!" );
    }

    @Test
    public void gettersCheck(){
        Vector3d vector = new Vector3d(1.0, 2.0, 3.0);
        assertEquals(1.0, vector.getX(),0.001);
        assertEquals(2.0, vector.getY(), 0.001);
        assertEquals(3.0, vector.getZ(), 0.001);
    }

    @Test
    public void magnitudeCheck(){
        Vector3d vector = new Vector3d(3.0, 4.0, 5.0);
        assertEquals(7.071, vector.magnitude(), 0.001);
    }

    @Test   
    public void dotProductCheck(){
        Vector3d vector1 = new Vector3d(1.0, 2.0, 3.0);
        Vector3d vector2 = new Vector3d(2.0, 3.0, 4.0);

        assertEquals(20.0, vector1.dotProduct(vector2), 0.001);
    }

    @Test
    public void addCheck(){
        Vector3d vector1 = new Vector3d(1.0, 2.0, 3.0);
        Vector3d vector2 = new Vector3d(2.0, 3.0, 4.0);
        Vector3d expected = new Vector3d(3.0, 5.0, 7.0);

        assertEquals(expected.getX(), vector1.add(vector2).getX(), 0.001);
        assertEquals(expected.getY(), vector1.add(vector2).getY(), 0.001);
        assertEquals(expected.getZ(), vector1.add(vector2).getZ(), 0.001);

    }








}